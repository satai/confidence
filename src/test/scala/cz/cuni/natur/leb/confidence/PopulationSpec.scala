package cz.cuni.natur.leb.confidence

import org.scalatest.ParallelTestExecution
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers
import org.scalatestplus.scalacheck.Checkers

class PopulationSpec extends AnyFunSpec with Matchers with Checkers with ParallelTestExecution {
  describe("Population") {

    it("random population has 1000 members") {
      Population.random.size should equal(1000)
    }

    it("random population members have all possible confidences") {
      Population.random
        .map(_.selfConfidence)
        .toSet[Int] should equal((0 to 100).toSet)
    }

  }
}


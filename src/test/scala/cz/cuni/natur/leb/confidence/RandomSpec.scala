package cz.cuni.natur.leb.confidence

import java.lang.Math.abs

import org.scalatest.ParallelTestExecution
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers
import org.scalatest.prop.TableDrivenPropertyChecks._
import org.scalatestplus.scalacheck.Checkers

class RandomSpec extends AnyFunSpec with Matchers with Checkers with ParallelTestExecution {
  describe("Random") {

    it("Random numbers are from 0 to 100 including this values") {
      (1 to 10000)
        .map(_ => Random.randomNum)
        .toSet[Int] should equal((0 to 100).toSet)
    }

  }
}


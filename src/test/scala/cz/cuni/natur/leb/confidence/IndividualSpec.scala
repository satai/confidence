package cz.cuni.natur.leb.confidence

import java.lang.Math.abs

import org.scalatest.funspec.AnyFunSpec
import org.scalatest.ParallelTestExecution
import org.scalatest.matchers.should.Matchers
import org.scalatestplus.scalacheck.Checkers
import org.scalatest.prop.TableDrivenPropertyChecks._

class IndividualSpec extends AnyFunSpec with Matchers with Checkers with ParallelTestExecution {
  describe("Individual") {

    it("has a nice string representation") {
      Individual.withSelfConfidence(42).toString should be("Individual(42)")
    }

    it("his diffidence is 100 - selfConfidence") {
      Individual.withSelfConfidence(42).diffidence should be(58)
    }

    it("can't be created with diffidence < 0") {
      intercept[AssertionError] {
        Individual.withDiffidence(-1)
      }
    }

    it("can't be created with self confidence < 0") {
      intercept[AssertionError] {
        Individual.withSelfConfidence(-1)
      }
    }

    it("can't be created with diffidence > 100") {
      intercept[AssertionError] {
        Individual.withDiffidence(101)
      }
    }

    it("can't be created with self confidence > 100") {
      intercept[AssertionError] {
        Individual.withSelfConfidence(101)
      }
    }

    it("bet expects hand to be <= 100") {
      intercept[AssertionError] {
        Individual.withSelfConfidence(1).bet(101)
      }
    }

    it("bet expects hand to be >= 0") {
      intercept[AssertionError] {
        Individual.withSelfConfidence(1).bet(101)
      }
    }

    it("bets twice as much iff his hand is at least value of his diffidence") {
      val values =
        Table(
          ("diffidence", "hand", "expectedBet"),
          (         42 ,    10 ,            1 ),
          (         42 ,    99 ,            2 ),
          (         42 ,   100 ,            2 ),
          (         42 ,    42 ,            2 ),
          (         42 ,    43 ,            2 ),
          (         42 ,    41 ,            1 ),
          (         42 ,     0 ,            1 ),
          (          0 ,     0 ,            2 ),
          (          0 ,     1 ,            2 ),
        )

      forAll (values) { (diffidence: Int, hand: Int, expectedBet: Int) =>
        Individual.withDiffidence(diffidence).bet(hand) should equal(expectedBet)
      }
    }

    it("mutant's self confidence is close to parent's one") {
      abs(Individual.withSelfConfidence(42).mutate().selfConfidence - 42) should be <= 1
    }

    it("mutant's self confidence is larger then parent's one sometimes") {
      (1 to 100)
        .map(_ => Individual.withSelfConfidence(42).mutate().selfConfidence)
        .count(_ > 42) should be > 20
    }

    it("mutant's self confidence is lower then parent's one sometimes") {
      (1 to 100)
        .map(_ => Individual.withSelfConfidence(42).mutate().selfConfidence)
        .count(_ < 42) should be > 20
    }

    it("mutant's self confidence can't be more then 100") {
      (1 to 100)
        .map(_ => Individual.withSelfConfidence(100).mutate().selfConfidence)
        .exists(_ > 100) should be(false)
    }

    it("mutant's self confidence can't be less then 0") {
      (1 to 100)
        .map(_ => Individual.withSelfConfidence(0).mutate().selfConfidence)
        .exists(_ < 0) should be(false)
    }

    it("random Individuals have all possible confidences") {
      (1 to 5000)
        .map(_ => Individual.random.selfConfidence)
        .toSet[Int] should equal((0 to 100 ).toSet)
    }
  }
}


package cz.cuni.natur.leb.confidence


object Population {

  /**
   * Populace is just an array of individuals
   */
  type Population = Array[Individual]

  /**
   * The size of an population is fixed to 1000.
   */
  val populationSize: Int = 1000

  def random: Population = (1 to populationSize).map(_ => Individual.random).toArray

}

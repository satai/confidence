package cz.cuni.natur.leb.confidence

import java.lang.Math.{abs, max, min}

import cz.cuni.natur.leb.confidence

import scala.util.Random

/**
 * Every individual has a value of self confidence. It's values are 0 to 100 including 0 and 100.
 */
class Individual(val selfConfidence: Int) {
  assume(selfConfidence <= 100, "selfConfidence of an individual must be in >=0 and <=100")
  assume(selfConfidence >= 0,   "selfConfidence of an individual must be in >=0 and <=100")

  /**
   * Individuals diffidence is opposite concept to his confidence. The value is 100 - selfConfidence
   */
  val diffidence: Int = 100 - selfConfidence

  /**
   * The individual doubles the possible outcome of a contest if his hand is at least equal his diffidence
   *
   * @param hand a number from 0 t0 100 including 0 and 100
   * @return 1 or 2
   */
  def bet(hand: Int): Int = {
    assume(hand <= 100, "hand must be in >=0 and <=100")
    assume(hand >= 0,   "hand must be in >=0 and <=100")

    if (hand >= diffidence) 2 else 1
  }

  /**
   * @return a new individual whose selfConfidence is the same or one more or one less
   */
  def mutate(): Individual = {
    val newSelfConfidenceCandidate =  selfConfidence + Random.nextInt(3) - 1
    val newSelfConfidence = (newSelfConfidenceCandidate max 0) min 100
    Individual.withSelfConfidence(newSelfConfidence)
  }

  override def toString = s"Individual($selfConfidence)"
}

object Individual {
  def random = withSelfConfidence(confidence.Random.randomNum)

  def withSelfConfidence(selfConfidence: Int): Individual = new Individual(selfConfidence)
  def withDiffidence(diffidence: Int): Individual = new Individual(100 - diffidence)
}

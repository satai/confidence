package cz.cuni.natur.leb.confidence

object Random {

  val random = scala.util.Random


  /**
   * Možné hodnoty jsou pak [0..100] včetně obou krajích možností.
   * Losované hodnoty jsou rozloženy rovnoměrně.
   */
  def randomNum = random.nextInt(100 + 1)
}
